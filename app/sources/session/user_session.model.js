const Sequelize = require('sequelize');
const schema = require('../../sequelize')


module.exports = schema.define('user_session',{
    user_id: {type: Sequelize.INTEGER, allowNull: false},
    token: {type: Sequelize.TEXT, allowNull: false},
    socket_id: {type: Sequelize.TEXT},
}, {
    timestamps: false,
    freezeTableName: true
})