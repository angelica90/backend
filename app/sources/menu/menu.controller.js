const {loadMenu} = require('./menu.model');

exports.getMenu = function(request, response){
	response.send(loadMenu());
}