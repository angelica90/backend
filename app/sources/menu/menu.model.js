
let loadMenu = () => JSON.parse(`{
    "menu" : [{
        "name" : "Usuarios",
        "icon" : "fas fa-users",
        "badge" : null,
        "menu" : [{
            "name" : "Crear usuarios",
            "icon" : "/img/2.png",
            "badge" : 0,
            "menu" : null
        },
        {
            "name" : "Modificar usuarios",
            "icon" : "/img/3.png",
            "badge" : 0,
            "menu" : null
        },
        {
            "name" : "Usuarios borrados",
            "icon" : "fas fa-user-minus",
            "badge" : 0,
            "menu" : null
        }]
    },
    {
        "name" : "Chat",
        "icon" : "fas fa-comment-dots",
        "badge" : 3,
        "menu" : [{
            "name" : "Mensajes",
            "icon" : "fas fa-envelope-square",
            "badge" : 3,
            "menu" : null
        },
        {
            "name" : "Amigos en línea",
            "icon" : "fas fa-user-friends",
            "badge" : 9,
            "menu" : null
        },
        {
            "name" : "Comunicados",
            "icon" : "/img/2.png",
            "badge" : 0,
            "menu" : null
        }]
    },
    {
        "name" : "Mi cuenta",
        "icon" : "far fa-address-card",
        "badge" : 3,
        "menu" : [{
            "name" : "Configuración",
            "icon" : "/img/2.png",
            "badge" : 0,
            "menu" : [{
                "name" : "Contraseña",
                "icon" : "",
                "badge" : null,
                "menu" : null
            },
            {
                "name" : "Correo",
                "icon" : "",
                "badge" : null,
                "menu" : null
            },
            {
                "name" : "Eliminar cuenta",
                "icon" : "",
                "badge" : null,
                "menu" : null
            }]
        },
        {
            "name" : "Mis datos personales",
            "icon" : "/img/2.png",
            "badge" : 0,
            "menu" : null
        },
        {
            "name" : "Cerrar sesión",
            "icon" : "fas fa-sign-out-out",
            "badge" : 0,
            "menu" : null
        }]
    }]
}`);

module.exports = {loadMenu};