const Chat = require('./chat.model');
const Sequelize = require('../../sequelize');
const JWT = require('./../../jwt');

const socketIO = require('./../../socket-io')

exports.write = function({query: { text, token, objective}}, response){
    var {id} = JWT.decodeToken(token);
    console.log(objective, text)
    objective = Number(objective);

    Chat.create({
        user_id: id,
        text:text
    }).then(item => {
        if(!objective) {
            response.status(200).send();
            socketIO.emiters.chat_updated(item.id)
        }
        else {

            socketIO.specific(socketIO.emiters.chat_updated, item.id, [objective]);
        }
    }).catch(error => {
        console.log(error);
        response.status(500).send(error)
    });

/*     Sequelize.query(`
    SELECT u.id, u.name, u.username 
    FROM user_session us JOIN users u ON u.id = us.user_id
    `,
    {type: Sequelize.QueryTypes.SELECT})
    .then(data => {
        response.status(200).send(data);
    })
    .catch(); */
}

exports.getList = function(request, response){

/*      Chat.findAll().belongsTo(User,{
         foreignKey: 'user_id'
     }).then(data => response.status(200).send(data))
     .catch(error => response.status(500).send(error)) */
     Sequelize.query(`
    SELECT u.name, u.username, c.text, c.text_time, c.id
    FROM chat c JOIN users u ON u.id = c.user_id
    `,
    {type: Sequelize.QueryTypes.SELECT})
    .then(data => {
        response.status(200).send(data);
    })
    .catch(error => response.status(400).send(error)); 
    /* Chat.findAll({
        limit: 50,
        order: [
            ['text_time', 'DESC']
        ]
    }).then(data => {
        response.status(200).send(data);
    }).catch(error => response.status(400).send(error)); */
}
exports.getItem = function({query: {id}}, response){
    Sequelize.query(`
    SELECT u.name, u.username, c.text, c.text_time, c.id
    FROM chat c JOIN users u ON u.id = c.user_id
    WHERE c.id = :id
    ORDER BY c.id ASC
    `,
    {
        type: Sequelize.QueryTypes.SELECT,
        replacements:  {
            id
        }
    })
    .then(data => {
        if(data.length)
            response.status(200).send(data[0]);
        else
            response.status(200).send(null);

    })
    .catch(error => response.status(400).send(error)); 
    /* Chat.findAll({
        limit: 50,
        order: [
            ['text_time', 'DESC']
        ]
    }).then(data => {
        response.status(200).send(data);
    }).catch(error => response.status(400).send(error)); */
}